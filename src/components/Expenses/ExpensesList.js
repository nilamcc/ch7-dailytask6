import React from 'react';
import './ExpensesList.css';
import ExpenseItem from './ExpenseItem';

const ExpenseList = (props) => {
    if (props.items.length === 0) {
        return <p className='expenses-list__fallback'>No Expense Found</p>;
    }
    return (
        <ul className="expenses__list">
            {props.items.map((item) => (
                <ExpenseItem
                    key={item.id}
                    title={item.title}
                    amount={item.amount}
                    date={item.date}
                />
            ))}
        </ul>
    )
}

export default ExpenseList;
